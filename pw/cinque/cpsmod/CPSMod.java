package pw.cinque.cpsmod;

import java.util.ArrayList;
import java.util.Iterator;
import java.io.Writer;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.FileNotFoundException;
import pw.cinque.CommandSettings.GuiSettings;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;
import net.minecraft.client.Minecraft;
import net.minecraft.command.ICommand;
import pw.cinque.CommandSettings.CommandSettings;
import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import java.util.List;
import net.minecraftforge.fml.common.Mod;

@Mod(name = "CPSMod", modid = "cpsmod", version = "1.0")
public class CPSMod
{
    private static List<Long> clicks;
    public static int cpsCounterPosX;
    public static boolean enabled;
    public static boolean preventDoubleclicks;
    public static Integer cpsCounterPosY;
    
    @Mod.EventHandler
    public void init(final FMLInitializationEvent event) {
        loadSettings();
        final ClickListener clickListener = new ClickListener();
        MinecraftForge.EVENT_BUS.register((Object)clickListener);
        MinecraftForge.EVENT_BUS.register((Object)new ClickCounterRenderer());
        FMLCommonHandler.instance().bus().register((Object)clickListener);
        ClientCommandHandler.instance.registerCommand((ICommand)new CommandSettings());
    }
    
    private static void loadSettings() {
        final File settings = new File(Minecraft.getMinecraft().mcDataDir, "cpsmod.settings");
        if (!settings.exists()) {
            return;
        }
        try {
            final BufferedReader reader = new BufferedReader(new FileReader(settings));
            final String[] options = reader.readLine().split(":");
            CPSMod.cpsCounterPosX = Integer.valueOf(options[0]);
            CPSMod.cpsCounterPosY = Integer.valueOf(options[1]);
            CPSMod.enabled = Boolean.valueOf(options[2]);
            GuiSettings.color = Integer.valueOf(options[3]);
            reader.close();
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e2) {
            e2.printStackTrace();
        }
        System.out.println(CPSMod.cpsCounterPosX);
    }
    
    public static void saveSettings() {
        final File settings = new File(Minecraft.getMinecraft().mcDataDir, "cpsmod.settings");
        try {
            final BufferedWriter writer = new BufferedWriter(new FileWriter(settings));
            writer.write(CPSMod.cpsCounterPosX + ":" + CPSMod.cpsCounterPosY + ":" + CPSMod.enabled + ":" + GuiSettings.color);
            writer.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public static void addClick() {
        CPSMod.clicks.add(System.currentTimeMillis());
    }
    
    public static int getClicks() {
        final Iterator<Long> iterator = CPSMod.clicks.iterator();
        while (iterator.hasNext()) {
            if (iterator.next() < System.currentTimeMillis() - 1000L) {
                iterator.remove();
            }
        }
        return CPSMod.clicks.size();
    }
    
    static {
        CPSMod.clicks = new ArrayList<Long>();
        CPSMod.cpsCounterPosX = 0;
        CPSMod.cpsCounterPosY = 0;
        CPSMod.enabled = true;
        GuiSettings.color = 15;
    }
}
