package pw.cinque.cpsmod;

import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.client.event.MouseEvent;

public class ClickListener
{
    private boolean hasClickedThisTick;
    
    public ClickListener() {
        this.hasClickedThisTick = false;
    }
    
    @SubscribeEvent
    public void onMouse(final MouseEvent event) {
        if (event.getButton() != 0) {
            return;
        }
        if (event.isButtonstate()) {
            this.hasClickedThisTick = true;
            CPSMod.addClick();
        }
    }
    
    @SubscribeEvent
    public void onClientTick(final TickEvent.ClientTickEvent event) {
        this.hasClickedThisTick = false;
    }
}
