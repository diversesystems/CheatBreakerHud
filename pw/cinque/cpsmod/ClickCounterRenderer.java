package pw.cinque.cpsmod;

import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.gui.Gui;
import org.lwjgl.opengl.GL11;
import pw.cinque.CommandSettings.GuiSettings;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import java.awt.Color;
import net.minecraft.client.Minecraft;

public class ClickCounterRenderer
{
    private final Minecraft mc;
    int index;
    long x;
    
    public static Color rainbowEffect(final float f, final float fade) {
        final float hue = (System.nanoTime() + f) / 4.0E9f % 1.0f;
        final long color = Long.parseLong(Integer.toHexString((int)Integer.valueOf(Color.HSBtoRGB(hue, 1.0f, 1.0f))), 16);
        final Color c = new Color((int)color);
        return new Color(c.getRed() / 255.0f * fade, c.getGreen() / 255.0f * fade, c.getBlue() / 255.0f * fade, c.getAlpha() / 255.0f);
    }
    
    public ClickCounterRenderer() {
        this.index = 0;
        this.x = 0L;
        this.mc = Minecraft.getMinecraft();
    }
    
    @SubscribeEvent
    public void onRenderGameOverlay(final RenderGameOverlayEvent event) {
        if (event.getType() != RenderGameOverlayEvent.ElementType.EXPERIENCE || event.isCancelable()) {
            return;
        }
        if (!CPSMod.enabled || (this.mc.currentScreen != null && !(this.mc.currentScreen instanceof GuiSettings)) || this.mc.gameSettings.showDebugInfo) {
            return;
        }
        if (this.mc.currentScreen != null || this.mc.gameSettings.showDebugInfo) {
            return;
        }
        final int cps = CPSMod.getClicks();
        int cpscolor = Integer.MAX_VALUE;
        if (GuiSettings.color == 0) {
            cpscolor = 2130706432;
        }
        else if (GuiSettings.color == 1) {
            cpscolor = 2130706602;
        }
        else if (GuiSettings.color == 2) {
            cpscolor = 2130749952;
        }
        else if (GuiSettings.color == 3) {
            cpscolor = 2130750122;
        }
        else if (GuiSettings.color == 4) {
            cpscolor = 2141847552;
        }
        else if (GuiSettings.color == 5) {
            cpscolor = 2141847722;
        }
        else if (GuiSettings.color == 6) {
            cpscolor = 2147461632;
        }
        else if (GuiSettings.color == 7) {
            cpscolor = 2141891242;
        }
        else if (GuiSettings.color == 8) {
            cpscolor = 2136298837;
        }
        else if (GuiSettings.color == 9) {
            cpscolor = 2136299007;
        }
        else if (GuiSettings.color == 10) {
            cpscolor = 2136342357;
        }
        else if (GuiSettings.color == 11) {
            cpscolor = 2136342527;
        }
        else if (GuiSettings.color == 12) {
            cpscolor = 2147439957;
        }
        else if (GuiSettings.color == 13) {
            cpscolor = 2147440127;
        }
        else if (GuiSettings.color == 14) {
            cpscolor = 2147483477;
        }
        else if (GuiSettings.color == 15) {
            cpscolor = Integer.MAX_VALUE;
        }
        else if (GuiSettings.color == 16) {
            cpscolor = rainbowEffect((float)this.index + this.x * 2000.0f, 1.0f).getRGB();
        }
        int cpswidth;
        if (CPSMod.getClicks() > 9) {
            cpswidth = 12;
        }
        else {
            cpswidth = 15;
        }
        final String text = cps + " CPS";
        final boolean blendEnabled = GL11.glIsEnabled(3042);
        GL11.glEnable(3042);
        Gui.drawRect(CPSMod.cpsCounterPosX, (int)CPSMod.cpsCounterPosY, CPSMod.cpsCounterPosX + 58, (int)CPSMod.cpsCounterPosY + 13, 2130706432);
        this.mc.fontRendererObj.drawString(text, CPSMod.cpsCounterPosX + cpswidth, (int)CPSMod.cpsCounterPosY + 3, cpscolor);
        if (blendEnabled) {
            GL11.glDisable(3042);
        }
    }
}
