package pw.cinque.CommandSettings;

import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.CommandBase;
import net.minecraft.server.MinecraftServer;

public class CommandSettings extends CommandBase
{
    public String getCommandName() {
        return "cpsmod";
    }
    
    public String getCommandUsage(final ICommandSender sender) {
        return "";
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        new GuiSettings().display();
    }

    public int getRequiredPermissionLevel() {
        return 0;
    }
    
    public boolean func_71519_b(final ICommandSender sender) {
        return true;
    }
}
