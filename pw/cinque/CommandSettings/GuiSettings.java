package pw.cinque.CommandSettings;

import pw.cinque.keystrokes.KeystrokesSettings;
import java.io.IOException;
import net.minecraft.client.gui.Gui;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import pw.cinque.keystrokes.Colors;
import pw.cinque.cpsmod.CPSMod;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;

public class GuiSettings extends GuiScreen
{
    public static int color;
    private int cps;
    private boolean isDragging;
    private int lastX;
    private int lastY;
    private GuiButton buttonReset;
    private GuiButton buttonAllign;
    private GuiButton buttonAllign2;
    private GuiButton buttonAllign3;
    private GuiButton buttonToggle;
    private GuiButton buttonColor;
    private GuiButton buttonColorPressed;
    
    public GuiSettings() {
        this.isDragging = false;
        this.lastX = 0;
        this.lastY = 0;
    }
    
    public void initGui() {
        this.buttonList.add(this.buttonReset = new GuiButton(0, this.width / 2 - 85, this.height / 2 - 70, 170, 20, "Reset Position"));
        this.buttonList.add(this.buttonAllign = new GuiButton(1, this.width / 2 - 85, this.height / 2 - 48, 170, 20, "Allign Under WASD"));
        this.buttonList.add(this.buttonAllign2 = new GuiButton(2, this.width / 2 - 85, this.height / 2 - 26, 170, 20, "Allign Under WASD_MOUSE"));
        this.buttonList.add(this.buttonAllign3 = new GuiButton(3, this.width / 2 - 85, this.height / 2 - 4, 170, 20, "Allign Under WASD_JUMP_MOUSE"));
        this.buttonList.add(this.buttonToggle = new GuiButton(4, this.width / 2 - 85, this.height / 2 - 93, 170, 20, "Enabled: " + CPSMod.enabled));
        this.buttonList.add(this.buttonColor = new GuiButton(5, this.width / 2 - 85, this.height / 2 + 18, 170, 20, "Color: " + Colors.values()[GuiSettings.color]));
    }
    
    public void display() {
        FMLCommonHandler.instance().bus().register((Object)this);
    }
    
    @SubscribeEvent
    public void onClientTick(final TickEvent.ClientTickEvent event) {
        FMLCommonHandler.instance().bus().unregister((Object)this);
        Minecraft.getMinecraft().displayGuiScreen((GuiScreen)this);
    }
    
    public void drawScreen(final int x, final int y, final float partialTicks) {
        super.drawDefaultBackground();
        final String text = this.cps + " CPS";
        Gui.drawRect(CPSMod.cpsCounterPosX, (int)CPSMod.cpsCounterPosY, CPSMod.cpsCounterPosX + 58, (int)CPSMod.cpsCounterPosY + 13, 1140850688);
        this.mc.fontRendererObj.drawString(text, CPSMod.cpsCounterPosX + 15, (int)CPSMod.cpsCounterPosY + 3, -1);
        super.drawScreen(x, y, partialTicks);
    }
    
    public void updateScreen() {
        this.cps = CPSMod.getClicks();
    }
    
    protected void keyTyped(final char c, final int key) {
        if (key == 1) {
            this.mc.displayGuiScreen((GuiScreen)null);
        }
    }
    
    protected void mouseClicked(final int x, final int y, final int time) throws IOException {
        final int minX = CPSMod.cpsCounterPosX;
        final int minY = (int)CPSMod.cpsCounterPosY;
        final int maxX = CPSMod.cpsCounterPosX + this.fontRendererObj.getStringWidth(this.cps + " CPS") + 30;
        final int maxY = CPSMod.cpsCounterPosY + 12;
        if (x >= minX && x <= maxX && y >= minY && y <= maxY) {
            this.isDragging = true;
            this.lastX = x;
            this.lastY = y;
        }
        super.mouseClicked(x, y, time);
    }
    
    protected void mouseReleased(final int x, final int y, final int which) {
        if (which == 0 && this.isDragging) {
            this.isDragging = false;
        }
        super.mouseReleased(x, y, which);
    }
    
    protected void mouseClickMove(final int x, final int y, final int lastButtonClicked, final long timeSinceClick) {
        if (this.isDragging) {
            CPSMod.cpsCounterPosX += x - this.lastX;
            CPSMod.cpsCounterPosY += y - this.lastY;
            this.lastX = x;
            this.lastY = y;
        }
        super.mouseClickMove(x, y, lastButtonClicked, timeSinceClick);
    }
    
    protected void actionPerformed(final GuiButton button) {
        if (button == this.buttonReset) {
            CPSMod.cpsCounterPosX = 0;
            CPSMod.cpsCounterPosY = 0;
        }
        if (button == this.buttonToggle) {
            CPSMod.enabled = !CPSMod.enabled;
            this.buttonToggle.displayString = "Enabled: " + CPSMod.enabled;
        }
        else if (button == this.buttonAllign) {
            CPSMod.cpsCounterPosX = KeystrokesSettings.keystrokesPosX + 1;
            CPSMod.cpsCounterPosY = KeystrokesSettings.keystrokesPosY + 41;
        }
        else if (button == this.buttonAllign2) {
            CPSMod.cpsCounterPosX = KeystrokesSettings.keystrokesPosX + 1;
            CPSMod.cpsCounterPosY = KeystrokesSettings.keystrokesPosY + 61;
        }
        else if (button == this.buttonAllign3) {
            CPSMod.cpsCounterPosX = KeystrokesSettings.keystrokesPosX + 1;
            CPSMod.cpsCounterPosY = KeystrokesSettings.keystrokesPosY + 75;
        }
        else if (button == this.buttonColor) {
            ++GuiSettings.color;
            if (GuiSettings.color == Colors.values().length) {
                GuiSettings.color = 0;
            }
            this.buttonColor.displayString = "Color: " + Colors.values()[GuiSettings.color];
        }
    }
    
    public void onGuiClosed() {
        CPSMod.saveSettings();
    }
    
    public boolean doesGuiPauseGame() {
        return false;
    }
    
    static {
        GuiSettings.color = 15;
    }
}
