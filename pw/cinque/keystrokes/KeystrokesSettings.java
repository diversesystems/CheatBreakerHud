package pw.cinque.keystrokes;

import java.io.Writer;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.Mod;
import net.minecraft.command.ICommand;
import io.fishermen.fpsdisplay.settings.CommandSettings;
import net.minecraftforge.client.ClientCommandHandler;
import pw.cinque.keystrokes.render.KeystrokesRenderer;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;

public class KeystrokesSettings
{
    public static int keystrokesPosX;
    public static int keystrokesPosY;
    
    public KeystrokesSettings() {
        KeystrokesSettings.keystrokesPosX = 0;
        KeystrokesSettings.keystrokesPosY = 0;
    }
    
    @Mod.EventHandler
    public void init(final FMLInitializationEvent event) {
        loadSettings();
        MinecraftForge.EVENT_BUS.register((Object)new KeystrokesRenderer());
        ClientCommandHandler.instance.registerCommand((ICommand)new CommandSettings());
    }
    
    static void loadSettings() {
        final File settings = new File(Minecraft.getMinecraft().mcDataDir, "keystrokes.settings");
        if (!settings.exists()) {
            return;
        }
        try {
            final BufferedReader reader = new BufferedReader(new FileReader(settings));
            final String[] options = reader.readLine().split(":");
            KeystrokesSettings.keystrokesPosX = Integer.valueOf(options[0]);
            KeystrokesSettings.keystrokesPosY = Integer.valueOf(options[1]);
            KeystrokesMod.enabled = Boolean.valueOf(options[2]);
            KeystrokesGui.color = Integer.valueOf(options[3]);
            KeystrokesGui.colorpressed = Integer.valueOf(options[4]);
            KeystrokesMod.mode = Integer.valueOf(options[5]);
            reader.close();
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e2) {
            e2.printStackTrace();
        }
        System.out.println(KeystrokesSettings.keystrokesPosX);
    }
    
    public static void saveSettings() {
        final File settings = new File(Minecraft.getMinecraft().mcDataDir, "keystrokes.settings");
        try {
            final BufferedWriter writer = new BufferedWriter(new FileWriter(settings));
            writer.write(KeystrokesSettings.keystrokesPosX + ":" + KeystrokesSettings.keystrokesPosY + ":" + KeystrokesMod.enabled + ":" + KeystrokesGui.color + ":" + KeystrokesGui.colorpressed + ":" + KeystrokesMod.mode);
            writer.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    static {
        KeystrokesGui.color = 15;
        KeystrokesGui.colorpressed = 0;
        KeystrokesMod.enabled = true;
        KeystrokesMod.mode = 0;
    }
}
