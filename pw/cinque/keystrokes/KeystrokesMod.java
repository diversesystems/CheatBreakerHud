package pw.cinque.keystrokes;

import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import pw.cinque.keystrokes.render.KeystrokesRenderer;
import net.minecraftforge.common.MinecraftForge;
import net.minecraft.command.ICommand;
import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.Mod;

@Mod(modid = "keystrokesmod", name = "KeystrokesMod", version = "0.1")
public class KeystrokesMod
{
    public static boolean displayGui;
    public static boolean enabled;
    public static int mode;
    private Minecraft mc;
    
    public KeystrokesMod() {
        this.mc = Minecraft.getMinecraft();
    }
    
    @Mod.EventHandler
    public void init(final FMLInitializationEvent event) {
        KeystrokesSettings.loadSettings();
        FMLCommonHandler.instance().bus().register((Object)this);
        ClientCommandHandler.instance.registerCommand((ICommand)new KeystrokesCommands());
        MinecraftForge.EVENT_BUS.register((Object)new KeystrokesRenderer());
    }
    
    @SubscribeEvent
    public void onTick(final TickEvent.ClientTickEvent event) {
        if (KeystrokesMod.displayGui && this.mc.currentScreen == null) {
            KeystrokesMod.displayGui = false;
            this.mc.displayGuiScreen((GuiScreen)new KeystrokesGui());
        }
    }
    
    static {
        KeystrokesMod.displayGui = false;
    }
}
