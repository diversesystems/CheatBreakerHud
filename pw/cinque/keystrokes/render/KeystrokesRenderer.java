package pw.cinque.keystrokes.render;

import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import java.util.Iterator;
import net.minecraft.client.gui.Gui;
import pw.cinque.keystrokes.KeystrokesSettings;
import org.lwjgl.opengl.GL11;
import pw.cinque.keystrokes.KeystrokesGui;
import pw.cinque.keystrokes.KeystrokesMod;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import java.awt.Color;
import net.minecraft.client.Minecraft;

public class KeystrokesRenderer
{
    private Minecraft mc;
    int index;
    long x;
    
    public KeystrokesRenderer() {
        this.index = 0;
        this.x = 0L;
        this.mc = Minecraft.getMinecraft();
    }
    
    public static Color rainbowEffect(final float f, final float fade) {
        final float hue = (System.nanoTime() + f) / 4.0E9f % 1.0f;
        final long color = Long.parseLong(Integer.toHexString((int)Integer.valueOf(Color.HSBtoRGB(hue, 1.0f, 1.0f))), 16);
        final Color c = new Color((int)color);
        return new Color(c.getRed() / 255.0f * fade, c.getGreen() / 255.0f * fade, c.getBlue() / 255.0f * fade, c.getAlpha() / 255.0f);
    }
    
    @SubscribeEvent
    public void onRender(final RenderGameOverlayEvent event) {
        if (!KeystrokesMod.enabled || (this.mc.currentScreen != null && !(this.mc.currentScreen instanceof KeystrokesGui)) || this.mc.gameSettings.showDebugInfo) {
            return;
        }
        if (event.isCancelable() || event.getType() != RenderGameOverlayEvent.ElementType.EXPERIENCE) {
            return;
        }
        if (event.getType() != RenderGameOverlayEvent.ElementType.EXPERIENCE || event.isCancelable()) {
            return;
        }
        if (this.mc.currentScreen != null || this.mc.gameSettings.showDebugInfo) {
            return;
        }
        final Mode mode = Mode.values()[KeystrokesMod.mode];
        int keycolor = Integer.MAX_VALUE;
        if (KeystrokesGui.color == 0) {
            keycolor = 2130706432;
        }
        else if (KeystrokesGui.color == 1) {
            keycolor = 2130706602;
        }
        else if (KeystrokesGui.color == 2) {
            keycolor = 2130749952;
        }
        else if (KeystrokesGui.color == 3) {
            keycolor = 2130750122;
        }
        else if (KeystrokesGui.color == 4) {
            keycolor = 2141847552;
        }
        else if (KeystrokesGui.color == 5) {
            keycolor = 2141847722;
        }
        else if (KeystrokesGui.color == 6) {
            keycolor = 2147461632;
        }
        else if (KeystrokesGui.color == 7) {
            keycolor = 2141891242;
        }
        else if (KeystrokesGui.color == 8) {
            keycolor = 2136298837;
        }
        else if (KeystrokesGui.color == 9) {
            keycolor = 2136299007;
        }
        else if (KeystrokesGui.color == 10) {
            keycolor = 2136342357;
        }
        else if (KeystrokesGui.color == 11) {
            keycolor = 2136342527;
        }
        else if (KeystrokesGui.color == 12) {
            keycolor = 2147439957;
        }
        else if (KeystrokesGui.color == 13) {
            keycolor = 2147440127;
        }
        else if (KeystrokesGui.color == 14) {
            keycolor = 2147483477;
        }
        else if (KeystrokesGui.color == 15) {
            keycolor = Integer.MAX_VALUE;
        }
        else if (KeystrokesGui.color == 16) {
            keycolor = rainbowEffect((float)this.index + this.x * 2000.0f, 1.0f).getRGB();
        }
        int keycolorpressed = 2130706432;
        if (KeystrokesGui.colorpressed == 0) {
            keycolorpressed = 2130706432;
        }
        else if (KeystrokesGui.colorpressed == 1) {
            keycolorpressed = 2130706602;
        }
        else if (KeystrokesGui.colorpressed == 2) {
            keycolorpressed = 2130749952;
        }
        else if (KeystrokesGui.colorpressed == 3) {
            keycolorpressed = 2130750122;
        }
        else if (KeystrokesGui.colorpressed == 4) {
            keycolorpressed = 2141847552;
        }
        else if (KeystrokesGui.colorpressed == 5) {
            keycolorpressed = 2141847722;
        }
        else if (KeystrokesGui.colorpressed == 6) {
            keycolorpressed = 2147461632;
        }
        else if (KeystrokesGui.colorpressed == 7) {
            keycolorpressed = 2141891242;
        }
        else if (KeystrokesGui.colorpressed == 8) {
            keycolorpressed = 2136298837;
        }
        else if (KeystrokesGui.colorpressed == 9) {
            keycolorpressed = 2136299007;
        }
        else if (KeystrokesGui.colorpressed == 10) {
            keycolorpressed = 2136342527;
        }
        else if (KeystrokesGui.colorpressed == 11) {
            keycolorpressed = 2136342357;
        }
        else if (KeystrokesGui.colorpressed == 12) {
            keycolorpressed = 2147439957;
        }
        else if (KeystrokesGui.colorpressed == 13) {
            keycolorpressed = 2147440127;
        }
        else if (KeystrokesGui.colorpressed == 14) {
            keycolorpressed = 2147483477;
        }
        else if (KeystrokesGui.colorpressed == 15) {
            keycolorpressed = Integer.MAX_VALUE;
        }
        else if (KeystrokesGui.colorpressed == 16) {
            keycolorpressed = rainbowEffect((float)this.index + this.x * 2000.0f, 1.0f).getRGB();
        }
        final boolean blend = GL11.glIsEnabled(3042);
        GL11.glDisable(3042);
        for (final Key key : mode.getKeys()) {
            final int textWidth = this.mc.fontRendererObj.getStringWidth(key.getName());
            Gui.drawRect(KeystrokesSettings.keystrokesPosX + key.getX(), KeystrokesSettings.keystrokesPosY + key.getY(), KeystrokesSettings.keystrokesPosX + key.getX() + key.getWidth(), KeystrokesSettings.keystrokesPosY + key.getY() + key.getHeight(), key.isDown() ? Integer.MAX_VALUE : 2130706432);
            this.mc.fontRendererObj.drawString(key.getName(), KeystrokesSettings.keystrokesPosX + key.getX() + key.getWidth() / 2 - textWidth / 2, KeystrokesSettings.keystrokesPosY + key.getY() + key.getHeight() / 2 - 4, key.isDown() ? keycolorpressed : keycolor);
        }
        if (blend) {
            GL11.glEnable(3042);
        }
    }
}
