package pw.cinque.keystrokes;

import java.io.IOException;
import java.util.Iterator;
import net.minecraft.client.gui.Gui;
import pw.cinque.keystrokes.render.Key;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import pw.cinque.keystrokes.render.Mode;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;

public class KeystrokesGui extends GuiScreen
{
    private boolean isDragging;
    private int lastX;
    private int lastY;
    public static int color;
    public static int colorpressed;
    int index;
    long x;
    private GuiButton buttonToggle;
    private GuiButton buttonLocation;
    private GuiButton buttonMode;
    private GuiButton buttonReset;
    private GuiButton buttonColor;
    private GuiButton buttonSave;
    private GuiButton buttonColorPressed;
    final Mode mode;
    
    public KeystrokesGui() {
        this.index = 0;
        this.x = 0L;
        this.mode = Mode.values()[KeystrokesMod.mode];
        this.isDragging = false;
        this.lastX = 0;
        this.lastY = 0;
    }
    
    public void initGui() {
        this.buttonList.add(this.buttonToggle = new GuiButton(0, this.width / 2 - 75, this.height / 2 - 62, 150, 20, "Enabled: " + KeystrokesMod.enabled));
        this.buttonList.add(this.buttonMode = new GuiButton(1, this.width / 2 - 75, this.height / 2 - 40, 150, 20, "Mode: " + Mode.values()[KeystrokesMod.mode]));
        this.buttonList.add(this.buttonReset = new GuiButton(2, this.width / 2 - 75, this.height / 2 - 18, 150, 20, "Reset Position"));
        this.buttonList.add(this.buttonColor = new GuiButton(3, this.width / 2 - 75, this.height / 2 + 4, 150, 20, "Color: " + Colors.values()[KeystrokesGui.color]));
        this.buttonList.add(this.buttonColorPressed = new GuiButton(4, this.width / 2 - 75, this.height / 2 + 26, 150, 20, "Color Pressed: " + Colors.values()[KeystrokesGui.colorpressed]));
    }
    
    public void display() {
        FMLCommonHandler.instance().bus().register((Object)this);
    }
    
    @SubscribeEvent
    public void onClientTick(final TickEvent.ClientTickEvent event) {
        FMLCommonHandler.instance().bus().unregister((Object)this);
        Minecraft.getMinecraft().displayGuiScreen((GuiScreen)this);
    }
    
    public void drawScreen(final int x, final int y, final float partialTicks) {
        super.drawDefaultBackground();
        for (final Key key : this.mode.getKeys()) {
            final int textWidth = this.mc.fontRendererObj.getStringWidth(key.getName());
            Gui.drawRect(KeystrokesSettings.keystrokesPosX + key.getX(), KeystrokesSettings.keystrokesPosY + key.getY(), KeystrokesSettings.keystrokesPosX + key.getX() + key.getWidth(), KeystrokesSettings.keystrokesPosY + key.getY() + key.getHeight(), key.isDown() ? Integer.MAX_VALUE : 2130706432);
            this.mc.fontRendererObj.drawString(key.getName(), KeystrokesSettings.keystrokesPosX + key.getX() + key.getWidth() / 2 - textWidth / 2, KeystrokesSettings.keystrokesPosY + key.getY() + key.getHeight() / 2 - 4, key.isDown() ? 2130706432 : Integer.MAX_VALUE);
            super.drawScreen(x, y, partialTicks);
        }
    }
    
    protected void actionPerformed(final GuiButton button) {
        if (button == this.buttonToggle) {
            KeystrokesMod.enabled = !KeystrokesMod.enabled;
            this.buttonToggle.displayString = "Enabled: " + KeystrokesMod.enabled;
        }
        else if (button == this.buttonReset) {
            KeystrokesSettings.keystrokesPosX = 0;
            KeystrokesSettings.keystrokesPosY = 0;
        }
        else if (button == this.buttonMode) {
            ++KeystrokesMod.mode;
            if (KeystrokesMod.mode == Mode.values().length) {
                KeystrokesMod.mode = 0;
            }
            this.buttonMode.displayString = "Mode: " + Mode.values()[KeystrokesMod.mode];
        }
        else if (button == this.buttonColor) {
            ++KeystrokesGui.color;
            if (KeystrokesGui.color == Colors.values().length) {
                KeystrokesGui.color = 0;
            }
            this.buttonColor.displayString = "Color: " + Colors.values()[KeystrokesGui.color];
        }
        else if (button == this.buttonColorPressed) {
            ++KeystrokesGui.colorpressed;
            if (KeystrokesGui.colorpressed == Colors.values().length) {
                KeystrokesGui.colorpressed = 0;
            }
            this.buttonColorPressed.displayString = "Color Pressed: " + Colors.values()[KeystrokesGui.colorpressed];
        }
    }
    
    protected void keyTyped(final char c, final int key) {
        if (key == 1) {
            this.mc.displayGuiScreen((GuiScreen)null);
        }
    }
    
    protected void mouseClicked(final int x, final int y, final int time) throws IOException {
        final int minX = KeystrokesSettings.keystrokesPosX;
        final int minY = KeystrokesSettings.keystrokesPosY;
        final int maxX = KeystrokesSettings.keystrokesPosX + 60;
        final int maxY = KeystrokesSettings.keystrokesPosY + 75;
        if (x >= minX && x <= maxX && y >= minY && y <= maxY) {
            this.isDragging = true;
            this.lastX = x;
            this.lastY = y;
        }
        super.mouseClicked(x, y, time);
    }
    
    protected void mouseReleased(final int x, final int y, final int which) {
        if (which == 0 && this.isDragging) {
            this.isDragging = false;
        }
        super.mouseReleased(x, y, which);
    }
    
    protected void mouseClickMove(final int x, final int y, final int lastButtonClicked, final long timeSinceClick) {
        if (this.isDragging) {
            KeystrokesSettings.keystrokesPosX += x - this.lastX;
            KeystrokesSettings.keystrokesPosY += y - this.lastY;
            this.lastX = x;
            this.lastY = y;
        }
        super.mouseClickMove(x, y, lastButtonClicked, timeSinceClick);
    }
    
    public void onGuiClosed() {
        KeystrokesSettings.saveSettings();
    }
    
    public boolean doesGuiPauseGame() {
        return false;
    }
}
