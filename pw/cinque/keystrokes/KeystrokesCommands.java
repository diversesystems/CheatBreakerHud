package pw.cinque.keystrokes;

import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.CommandBase;
import net.minecraft.server.MinecraftServer;

public class KeystrokesCommands extends CommandBase
{
    public String getCommandName() {
        return "keystrokes";
    }
    
    public String getCommandUsage(final ICommandSender sender) {
        return "";
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        KeystrokesMod.displayGui = true;
    }

    public int getRequiredPermissionLevel() {
        return 0;
    }
    
    public boolean func_71519_b(final ICommandSender sender) {
        return true;
    }
}
