package io.fishermen.fpsdisplay;

import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.gui.Gui;
import org.lwjgl.opengl.GL11;
import pw.cinque.cpsmod.CPSMod;
import io.fishermen.fpsdisplay.settings.GuiSettings;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import java.awt.Color;
import net.minecraft.client.Minecraft;

public class FPSRenderer
{
    private final Minecraft mc;
    int index;
    long x;
    
    public static Color rainbowEffect(final float f, final float fade) {
        final float hue = (System.nanoTime() + f) / 4.0E9f % 1.0f;
        final long color = Long.parseLong(Integer.toHexString((int)Integer.valueOf(Color.HSBtoRGB(hue, 1.0f, 1.0f))), 16);
        final Color c = new Color((int)color);
        return new Color(c.getRed() / 255.0f * fade, c.getGreen() / 255.0f * fade, c.getBlue() / 255.0f * fade, c.getAlpha() / 255.0f);
    }
    
    public FPSRenderer() {
        this.index = 0;
        this.x = 0L;
        this.mc = Minecraft.getMinecraft();
    }
    
    @SubscribeEvent
    public void onRenderGameOverlay(final RenderGameOverlayEvent event) {
        if (event.getType() != RenderGameOverlayEvent.ElementType.EXPERIENCE || event.isCancelable()) {
            return;
        }
        if (!FPSDisplay.enabled || (this.mc.currentScreen != null && !(this.mc.currentScreen instanceof GuiSettings)) || this.mc.gameSettings.showDebugInfo) {
            return;
        }
        if (this.mc.currentScreen != null || this.mc.gameSettings.showDebugInfo) {
            return;
        }
        final int cps = CPSMod.getClicks();
        int fpscolor = Integer.MAX_VALUE;
        if (FPSDisplay.color == 0) {
            fpscolor = 2130706432;
        }
        else if (FPSDisplay.color == 1) {
            fpscolor = 2130706602;
        }
        else if (FPSDisplay.color == 2) {
            fpscolor = 2130749952;
        }
        else if (FPSDisplay.color == 3) {
            fpscolor = 2130750122;
        }
        else if (FPSDisplay.color == 4) {
            fpscolor = 2141847552;
        }
        else if (FPSDisplay.color == 5) {
            fpscolor = 2141847722;
        }
        else if (FPSDisplay.color == 6) {
            fpscolor = 2147461632;
        }
        else if (FPSDisplay.color == 7) {
            fpscolor = 2141891242;
        }
        else if (FPSDisplay.color == 8) {
            fpscolor = 2136298837;
        }
        else if (FPSDisplay.color == 9) {
            fpscolor = 2136299007;
        }
        else if (FPSDisplay.color == 10) {
            fpscolor = 133521407;
        }
        else if (FPSDisplay.color == 11) {
            fpscolor = 2136342357;
        }
        else if (FPSDisplay.color == 12) {
            fpscolor = 2147439957;
        }
        else if (FPSDisplay.color == 13) {
            fpscolor = 2147440127;
        }
        else if (FPSDisplay.color == 14) {
            fpscolor = 2147483477;
        }
        else if (FPSDisplay.color == 15) {
            fpscolor = Integer.MAX_VALUE;
        }
        else if (FPSDisplay.color == 16) {
            fpscolor = rainbowEffect((float)this.index + this.x * 2000.0f, 1.0f).getRGB();
        }
        final String text = " " + this.mc.debug.split("[( f]")[0].replace((CharSequence)"fps", (CharSequence)"FPS ") + " FPS  ";
        final boolean blendEnabled = GL11.glIsEnabled(3042);
        GL11.glEnable(3042);
        Gui.drawRect(FPSDisplay.counterPosX, FPSDisplay.counterPosY, FPSDisplay.counterPosX + 58, FPSDisplay.counterPosY + 13, 2130706432);
        this.mc.fontRendererObj.drawString(text, FPSDisplay.counterPosX + 6, FPSDisplay.counterPosY + 3, fpscolor);
        if (blendEnabled) {
            GL11.glDisable(3042);
        }
    }
}
