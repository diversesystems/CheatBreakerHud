package io.fishermen.fpsdisplay.settings;

import java.io.IOException;
import net.minecraft.client.gui.Gui;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import pw.cinque.keystrokes.Colors;
import io.fishermen.fpsdisplay.FPSDisplay;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;

public class GuiSettings extends GuiScreen
{
    private boolean isDragging;
    private int lastX;
    private int lastY;
    private GuiButton buttonToggle;
    private GuiButton buttonReset;
    private GuiButton buttonColor;
    
    public GuiSettings() {
        this.isDragging = false;
        this.lastX = 0;
        this.lastY = 0;
    }
    
    public void initGui() {
        this.buttonList.add(this.buttonReset = new GuiButton(0, this.width / 2 - 75, this.height / 2 - 22, 150, 20, "Reset Position"));
        this.buttonList.add(this.buttonToggle = new GuiButton(1, this.width / 2 - 75, this.height / 2 - 44, 150, 20, "Enabled: " + FPSDisplay.enabled));
        this.buttonList.add(this.buttonColor = new GuiButton(2, this.width / 2 - 75, this.height / 2 + 0, 150, 20, "Color: " + Colors.values()[FPSDisplay.color]));
    }
    
    public void display() {
        FMLCommonHandler.instance().bus().register((Object)this);
    }
    
    @SubscribeEvent
    public void onClientTick(final TickEvent.ClientTickEvent event) {
        FMLCommonHandler.instance().bus().unregister((Object)this);
        Minecraft.getMinecraft().displayGuiScreen((GuiScreen)this);
    }
    
    public void drawScreen(final int x, final int y, final float partialTicks) {
        super.drawDefaultBackground();
        final String text = " " + this.mc.debug.split("[( f]")[0].replace((CharSequence)"fps", (CharSequence)"FPS ") + " FPS  ";
        Gui.drawRect(FPSDisplay.counterPosX, FPSDisplay.counterPosY, FPSDisplay.counterPosX + 58, FPSDisplay.counterPosY + 13, 1140850688);
        this.mc.fontRendererObj.drawString(text, FPSDisplay.counterPosX + 6, FPSDisplay.counterPosY + 3, -1);
        super.drawScreen(x, y, partialTicks);
    }
    
    protected void keyTyped(final char c, final int key) {
        if (key == 1) {
            this.mc.displayGuiScreen((GuiScreen)null);
        }
    }
    
    protected void mouseClicked(final int x, final int y, final int time) throws IOException {
        final int minX = FPSDisplay.counterPosX;
        final int minY = FPSDisplay.counterPosY;
        final int maxX = FPSDisplay.counterPosX + this.fontRendererObj.getStringWidth(this.mc.debug.split(",")[0].replace((CharSequence)"fps", (CharSequence)"fps")) + 30;
        final int maxY = FPSDisplay.counterPosY + 12;
        if (x >= minX && x <= maxX && y >= minY && y <= maxY) {
            this.isDragging = true;
            this.lastX = x;
            this.lastY = y;
        }
        super.mouseClicked(x, y, time);
    }
    
    protected void mouseReleased(final int x, final int y, final int which) {
        if (which == 0 && this.isDragging) {
            this.isDragging = false;
        }
        super.mouseReleased(x, y, which);
    }
    
    protected void mouseClickMove(final int x, final int y, final int lastButtonClicked, final long timeSinceClick) {
        if (this.isDragging) {
            FPSDisplay.counterPosX += x - this.lastX;
            FPSDisplay.counterPosY += y - this.lastY;
            this.lastX = x;
            this.lastY = y;
        }
        super.mouseClickMove(x, y, lastButtonClicked, timeSinceClick);
    }
    
    protected void actionPerformed(final GuiButton button) {
        if (button == this.buttonReset) {
            FPSDisplay.counterPosX = 0;
            FPSDisplay.counterPosY = 0;
        }
        if (button == this.buttonToggle) {
            FPSDisplay.enabled = !FPSDisplay.enabled;
            this.buttonToggle.displayString = "Enabled: " + FPSDisplay.enabled;
        }
        else if (button == this.buttonColor) {
            ++FPSDisplay.color;
            if (FPSDisplay.color == Colors.values().length) {
                FPSDisplay.color = 0;
            }
            this.buttonColor.displayString = "Color: " + Colors.values()[FPSDisplay.color];
        }
    }
    
    public void onGuiClosed() {
        FPSDisplay.saveSettings();
    }
    
    public boolean doesGuiPauseGame() {
        return false;
    }
}
