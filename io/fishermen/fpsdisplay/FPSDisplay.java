package io.fishermen.fpsdisplay;

import java.io.Writer;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;
import net.minecraft.client.Minecraft;
import net.minecraft.command.ICommand;
import io.fishermen.fpsdisplay.settings.CommandSettings;
import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.Mod;

@Mod(name = "FPSDisplayMod", modid = "fpsdisplaymod", version = "1.0")
public class FPSDisplay
{
    public static int counterPosX;
    public static int color;
    public static boolean preventDoubleclicks;
    public static int counterPosY;
    public static boolean enabled;
    
    public FPSDisplay() {
        FPSDisplay.counterPosX = 0;
        FPSDisplay.counterPosY = 0;
    }
    
    @Mod.EventHandler
    public void init(final FMLInitializationEvent event) {
        loadSettings();
        MinecraftForge.EVENT_BUS.register((Object)new FPSRenderer());
        ClientCommandHandler.instance.registerCommand((ICommand)new CommandSettings());
    }
    
    private static void loadSettings() {
        final File settings = new File(Minecraft.getMinecraft().mcDataDir, "fpsdisplaymod.settings");
        if (!settings.exists()) {
            return;
        }
        try {
            final BufferedReader reader = new BufferedReader(new FileReader(settings));
            final String[] options = reader.readLine().split(":");
            FPSDisplay.counterPosX = Integer.valueOf(options[0]);
            FPSDisplay.counterPosY = Integer.valueOf(options[1]);
            FPSDisplay.enabled = Boolean.valueOf(options[2]);
            FPSDisplay.color = Integer.valueOf(options[3]);
            reader.close();
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e2) {
            e2.printStackTrace();
        }
        System.out.println(FPSDisplay.counterPosX);
    }
    
    public static void saveSettings() {
        final File settings = new File(Minecraft.getMinecraft().mcDataDir, "fpsdisplaymod.settings");
        try {
            final BufferedWriter writer = new BufferedWriter(new FileWriter(settings));
            writer.write(FPSDisplay.counterPosX + ":" + FPSDisplay.counterPosY + ":" + FPSDisplay.enabled + ":" + FPSDisplay.color);
            writer.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    static {
        FPSDisplay.enabled = true;
        FPSDisplay.color = 15;
    }
}
